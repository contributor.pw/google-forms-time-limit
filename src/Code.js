/* exported runTimerLimit disableAcceptingResponses SETTINGS onOpen viewStatus */

const SETTINGS = {
  minutesLimit: 40,
};

// UserActions

/**
 * Runs the Timer
 */
function runTimerLimit() {
  const form = FormApp.getActiveForm();
  form.setAcceptingResponses(true);
  deleteTrigger();
  ScriptApp.newTrigger('disableAcceptingResponses')
    .timeBased()
    .after(SETTINGS.minutesLimit * 60 * 1000)
    .create();
  const dateRun = new Date();
  const dateDisable = new Date(dateRun.getTime() + SETTINGS.minutesLimit * 60 * 1000);
  PropertiesService.getDocumentProperties().setProperties({
    'date-run': dateToString_(dateRun),
    'date-disable': dateToString_(dateDisable),
  });
}

/**
 * Disables accepting responses
 *
 */
function disableAcceptingResponses() {
  const form = FormApp.getActiveForm();
  form.setAcceptingResponses(false);
  deleteTrigger();
}

/**
 * Removes running triggers
 * @returns {void}
 */
function deleteTrigger() {
  ScriptApp.getProjectTriggers()
    .filter(
      (trigger) =>
        trigger.getEventType() === ScriptApp.EventType.CLOCK &&
        trigger.getHandlerFunction() === 'disableAcceptingResponses',
    )
    .forEach((trigger) => ScriptApp.deleteTrigger(trigger));
}

// App

/**
 * Displays a side menu
 */
function viewStatus_() {
  const status = getStatus_();
  const html = `
<div>
  <p><b>Time limit: </b><?!= minutesLimit ?></p>
  <p><b>Last run time: </b><?!= dateRun ?></p>
  <p><b>Disable time: </b><?!= dateDisable ?></p>
  <p><b>Is running now?: </b><?!= isRunning ?></p>
</div>
  `;
  const template = HtmlService.createTemplate(html);
  template.dateRun = status['date-run'];
  template.dateDisable = status['date-disable'];
  template.minutesLimit = status.minutesLimit;
  template.isRunning = status.isRunning;
  const userInterface = template.evaluate();
  FormApp.getUi().showSidebar(userInterface);
}

/**
 * Gets current status of the App
 *
 * @returns {{
 *   'date-run': string;
 *   'date-disable': string;
 *   isRunning: boolean;
 * }}
 */
function getStatus_() {
  const props = PropertiesService.getDocumentProperties().getProperties();
  const isRunning = ScriptApp.getProjectTriggers().some(
    (trigger) => trigger.getHandlerFunction() === 'disableAcceptingResponses',
  );
  return { ...props, minutesLimit: SETTINGS.minutesLimit, isRunning };
}

// Ui

function onOpen() {
  FormApp.getUi()
    .createMenu('Timer')
    .addItem('Run timer', 'runTimerLimit')
    .addItem('Timer status', 'viewStatus_')
    .addToUi();
}

// Tools

/**
 * Converts a date to a readable string
 *
 * @param {Date | string | number} date
 * @returns {string}
 */
function dateToString_(date) {
  return Utilities.formatDate(new Date(date), CalendarApp.getTimeZone(), 'yyyy-MM-dd hh:mm');
}
